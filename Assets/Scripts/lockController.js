﻿public var sym = 0;
var symTexs = new Texture[5];

function Start () {
	symTexs[0] = Resources.Load("locksym1") as Texture;
	symTexs[1] = Resources.Load("locksym2") as Texture;
	symTexs[2] = Resources.Load("locksym3") as Texture;
	symTexs[3] = Resources.Load("locksym4") as Texture;
	symTexs[4] = Resources.Load("locksym5") as Texture;

}

function OnMouseDown() {
	var p = GameObject.FindGameObjectWithTag("Player");
	var dist = (transform.position - p.transform.position).sqrMagnitude;
	var reach = 8;
	if (dist <= reach) {
		sym ++;
		sym = sym%5;
		renderer.material.mainTexture = symTexs[sym];
		audio.Play();
	}
}