private var motor : CharacterMotor;
var animator: Animator;
// Use this for initialization
function Start () {
    animator = GetComponent("Animator");
}

// Use this for initialization
function Awake () {
	motor = GetComponent(CharacterMotor);
}
function LateUpdate () {
    if (Input.GetKey ("w") || Input.GetKey ("s")){
       animator.SetBool("char_Walking", true);
    }else{
    	animator.SetBool("char_Walking", false);
    }
    if (Input.GetKey ("d") || Input.GetKey ("a")){
       animator.SetBool("char_walk_left", true);
    }else{
    	animator.SetBool("char_walk_left", false);
    }
}
// Update is called once per frame
function Update () {
	// Get the input vector from keyboard or analog stick
	var directionVector = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
	
	if (directionVector != Vector3.zero) {
		// Get the length of the directon vector and then normalize it
		// Dividing by the length is cheaper than normalizing when we already have the length anyway
		var directionLength = directionVector.magnitude;
		directionVector = directionVector / directionLength;
		
		// Make sure the length is no bigger than 1
		directionLength = Mathf.Min(1, directionLength);
		
		// Make the input vector more sensitive towards the extremes and less sensitive in the middle
		// This makes it easier to control slow speeds when using analog sticks
		directionLength = directionLength * directionLength;
		
		// Multiply the normalized direction vector by the modified length
		directionVector = directionVector * directionLength;
	}
	
	// Apply the direction to the CharacterMotor
	motor.inputMoveDirection = transform.rotation * directionVector;
	motor.inputJump = Input.GetButton("Jump");
}

// Require a character controller to be attached to the same game object
@script RequireComponent (CharacterMotor)
@script AddComponentMenu ("Character/FPS Input Controller")
