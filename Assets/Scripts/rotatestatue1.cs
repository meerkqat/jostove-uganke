﻿using UnityEngine;
using System.Collections;
public class rotatestatue1 : MonoBehaviour {


	private bool playingAudio = false;
	private bool lClick = false;
	// Update is called once per frame
	void Update () {
		var ps = GameObject.FindGameObjectsWithTag("Player");
		var p = ps[0];
		var dist = (transform.position - p.transform.position).sqrMagnitude;
		var reach = 8;

		//var s1 = GameObject.FindGameObjectWithTag("Statue1");
		//var s2 = GameObject.FindGameObjectWithTag("Statue2");

		if (Input.GetMouseButtonDown (0)) 
			lClick = true; 

		if (Input.GetMouseButtonUp (0)) 
			lClick = false; 

		if (lClick && dist <= reach) {
			transform.Rotate (new Vector3 (0, 0.8f, 0), Space.World);
			if(playingAudio == false){
				audio.Play();
				playingAudio = true;
			}
			//Debug.Log (s1.renderer.transform.rotation.y);
			//Debug.Log (s2.renderer.transform.rotation.y);
			//Debug.Log ("----------------");
		} else if (lClick == false){
			//this.audio.Play = false;
			if(playingAudio){
				audio.Pause();
			}
			playingAudio = false;
		}

	}
}

