﻿function Update(){
	if (Input.GetKeyDown(KeyCode.Escape)) {
		Application.LoadLevel(0);
	}
}

function OnControllerColliderHit (hit : ControllerColliderHit) {
	var body : Rigidbody = hit.collider.attachedRigidbody;

	if (body == null)
		return;
	if (body.gameObject.name.Equals("levelExit"))
		Application.LoadLevel(3);
}