﻿var Win32HeavyCursor : Texture2D;
var cursorMode : CursorMode = CursorMode.Auto;
var hotSpot : Vector2 = Vector2.zero;
var ps : GameObject[];
var cursor_normal : GUITexture;
var cursor_touch : GUITexture;

// Use this for initialization
function Start () {

}
function OnMouseOver() {
	//GUI.DrawTexture(Event.current.mousePosition, mousecursor);
	
	ps = GameObject.FindGameObjectsWithTag("Player");

	var p = ps[0];
	var dist = (transform.position - p.transform.position).sqrMagnitude;
	var reach = 8;
	if (dist <= reach) {	
		GameObject.Find("cursor_touch").guiTexture.enabled = true;
		GameObject.Find("cursor_normal").guiTexture.enabled = false;
	}
}
function OnMouseExit () {
	GameObject.Find("cursor_touch").guiTexture.enabled = false;
	GameObject.Find("cursor_normal").guiTexture.enabled = true;
}

	

