﻿	

     
    var isQuitButton = false;
    var isPlayButton = false;
    var isEggButton = false;
     
    var eggActive = false;
     
    function OnMouseEnter() {
            renderer.material.color = Color.green;
           
    }
    function OnMouseExit() {
            renderer.material.color = Color.white;
           
    }
    function OnMouseUp() {
            if(isQuitButton){
                    Application.Quit();
           
            }else if(isPlayButton){
                    Application.LoadLevel(1);
           
            }else if(isEggButton){
                    var p1 = GameObject.FindGameObjectWithTag("Player");
                    if(eggActive){
                            p1.GetComponent("FPSInputController").enabled = false;
                            p1.GetComponent("MouseLook").enabled = false;
                            eggActive = false;
                    }else{
                            p1.GetComponent("FPSInputController").enabled = true;
                            p1.GetComponent("MouseLook").enabled = true;
                            eggActive = true;
                    }
     
            }
     
    }


