var anim;

function Start () {
	anim = GetComponent(Animator);
	anim.SetBool("lift", false);
}

function Update () {
	var l1 = GameObject.FindGameObjectWithTag("lSym1").GetComponent("lockController");
	var l2 = GameObject.FindGameObjectWithTag("lSym2").GetComponent("lockController");
	var l3 = GameObject.FindGameObjectWithTag("lSym3").GetComponent("lockController");
	
	if(l1.sym == 1 && l2.sym == 3 && l3.sym == 2) {
		anim.SetBool("lift", true);
		audio.Play();
	}
}