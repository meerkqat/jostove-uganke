﻿using UnityEngine;
using System.Collections;

public class CameraChoose : MonoBehaviour {
	int activeCam;
	// Use this for initialization
	void Start () {
		activeCam = 1;
	}

	// Update is called once per frame
	void Update () {
		var c1 = GameObject.FindGameObjectWithTag("MainCamera");
		var c2 = GameObject.FindGameObjectWithTag("SecondCamera");

		if (Input.GetKeyDown ("c")){
			if (activeCam == 1) {
				c1.camera.enabled = false;
				c2.camera.enabled = true;
				activeCam = 2;	
			}else if(activeCam == 2) {
				c1.camera.enabled = true;
				c2.camera.enabled = false;
				activeCam = 1;	
			}
		
		}
	}
}
