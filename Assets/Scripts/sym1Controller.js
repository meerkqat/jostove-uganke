﻿function Update () {
	var gos = GameObject.FindGameObjectsWithTag("pPlate");
	var allPressed = true;
	for (var g in gos) {
		allPressed = allPressed && g.GetComponent("plateController").pressed;
	}

	if (allPressed) {
		renderer.enabled = true;
	} else {
		renderer.enabled = false;
	}
}