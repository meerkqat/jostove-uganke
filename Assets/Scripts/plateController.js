﻿
public var pressed = false;
var anim;

function Start () {
	anim = GetComponent(Animator);
}

function OnCollisionEnter(collision : Collision ) {
	//Debug.Log (collision.gameObject.name);
	pressed = true;
	//anim.SetBool("hasObject", true);
}

function OnCollisionExit(collision: Collision ) {
	//Debug.Log (collision.gameObject.name);
	pressed = false;
	//anim.SetBool("hasObject", true);
}

function Update () {
	anim.SetBool("hasObject", pressed);
}

