﻿using UnityEngine;
using System;
using System.Collections;

public class grobnica_dvig : MonoBehaviour {
	Animator anim;
	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator>();
	}
	Boolean dvignjen = false;
	
	// Update is called once per frame
	void Update () {
		var s1 = GameObject.FindGameObjectWithTag("Statue1");
		var s2 = GameObject.FindGameObjectWithTag("Statue2");

		// s2 -> 80-90
		// s1 -> 235 - 245
		var y1 = Math.Abs (s1.renderer.transform.rotation.y);
		var y2 = Math.Abs (s2.renderer.transform.rotation.y);

		//Debug.Log (y1);
		//Debug.Log (y2);
		//Debug.Log ("----------------");

		if(y1 > 0.1 && y1 < 0.2 && y2 > 0.9){
			if(dvignjen == false){
				dvignjen = true;
				/*Vector3 tmp = this.transform.position;;
				tmp.y += 1.5f;
				this.transform.position = tmp; */
				anim.SetBool("Rise", true);

			}
		}else if(dvignjen == true){
			dvignjen = false;
			anim.SetBool("Rise", false);

		}
	}
}
